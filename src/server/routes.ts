import * as express from 'express';
import DB from './db';

const router = express.Router();

router.get('/api/product-categories', async(req, res) => {
    try {
        let productCategories = await DB.ProductCategories.getCategories();
        res.json(productCategories);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/products-by-category/:id', async(req, res) => {
    try {
        let products = await DB.Products.getProductsByCategory(req.params.id);
        res.json(products);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/product-by-id/:id', async(req, res) => {
    try {
        let product = await DB.Products.productById(req.params.id);
        res.json(product[0]);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/product-save', async(req, res) => {
    try {
        req.on('data', function(data) {
            let params = JSON.parse(data.toString());
            let product = {
                productName: params.productName,
                productDescription: params.productDescription
            };
            DB.Products.saveProduct(product);
        })
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/category-by-id/:categoryId', async(req, res) => {
    try {
        let category = await DB.Categories.categoryById(req.params.categoryId);
        res.json(category);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/category-exists/:categoryName', async(req, res) => {
    try {
        let category = await DB.Categories.categoryExists(req.params.categoryName);
        let result = category.length ? true : false;
        res.json(result);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/category-save', async(req, res) => {
    try {
        req.on('data', function(data) {
            let params = JSON.parse(data.toString());
            let category = {
                categoryName: params.categoryName
            };
            DB.Categories.saveCategory(category);
        })
        res.json("success");
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/category-update/:id/:updateCategory', async(req, res) => {
    try {
        req.on('data', function(data) {
            let category = {
                id: req.params.id,
                categoryName: req.params.updateCategory
            };
            DB.Categories.updateCategory(category);
        })
        res.json("success");
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/category-delete', async(req, res) => {
    try {
        req.on('data', function(data) {
            let id = JSON.parse(data.toString());
            DB.Object.deleteObject("categories", "id", id);
        })
        res.json("success");
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/customers', async(req, res) => {
    try {
        let customers = await DB.Customers.getCustomers();
        res.json(customers);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/get-next-customerNumber', async(req, res) => {
    try {
        let customerNumberNext = 0;
        let customer = await DB.Customers.getNextCustomerNumber();
        if(customer) {
            customerNumberNext = customer[0].customerNumber + 1;
        }
        res.json(customerNumberNext);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/customer-save', async(req, res) => {
    try {
        req.on('data', function(data) {
            let params = JSON.parse(data.toString());
            DB.Object.insertObject(
                "customers",
                {
                    customerNumber: params.customerNumber,
                    salutation: params.customerSalutation,
                    firstname: params.customerFirstname,
                    lastname: params.customerLastname
                }
            );
        })
        res.json("success");
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/customer-delete/:id', async(req, res) => {
    try {
        let deleteObject = DB.Object.deleteObject("customers", "customerNumber", req.params.id);
        res.json(deleteObject);
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/customer-by-id/:id', async(req, res) => {
    try {
        let customer = await DB.Customers.getCustomerById(req.params.id);
        res.json(customer[0]);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.post('/api/customer-update/:customerNumber', async(req, res) => {
    try {
        req.on('data', function(data) {
            let category = {
                customerNumber: req.params.customerNumber
            };
            DB.Customers.updateCustomer(category);
        })
        res.json("success");
    } catch (error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/customer-name/:customerNumber', async(req, res) => {
    try {
        let customer = await DB.Customers.getCustomerName(req.params.customerNumber);
        res.json(customer);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/offer-years', async(req, res) => {
    try {
        let offers = await DB.Assignments.getAssignmentYears(1);
        res.json(offers);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/assignment-years', async(req, res) => {
    try {
        let assignments = await DB.Assignments.getAssignmentYears(2);
        res.json(assignments);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/assignments-by-year/:year/:assignment', async(req, res) => {
    try {
        let assignments = await DB.Assignments.getAssignmentsByYearList(req.params.year, req.params.assignment);
        res.json(assignments);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

router.get('/api/bill-years', async(req, res) => {
    try {
        let bills = await DB.Assignments.getAssignmentYears(3);
        res.json(bills);
    } catch(error) {
        console.log(error);
        res.sendStatus(500);
    }
});

export default router;