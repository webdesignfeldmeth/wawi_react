import functions from './functions';
import object from './object';

export const getCustomers = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT customerNumber, firstname, lastname FROM customers ORDER BY lastname ASC";
        let sql_var = [];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const getCustomerById = async (id) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM customers WHERE (customerNumber = ?)";
        let sql_var = [id];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const getCustomerName = async (customerNumber) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT firstname, lastname FROM customers " +
                    "WHERE customerNumber = ?";
        let sql_var = [customerNumber];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const getNextCustomerNumber = () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM customers ORDER BY customerNumber DESC LIMIT 1";
        let sql_var = [
        ];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const updateCustomer = (customer) => {
    console.log(customer);
    // return new Promise((resolve, reject) => {
    //     let sql = "UPDATE customers SET " + 
    //                     "company = ?" +
    //                     "firstname = ?"
    //                     "WHERE id = ?";
    //     let sql_var = [
    //         customer.company,
    //         customer.firstname
    //     ];

    //     functions.executeQuery(resolve, reject, sql, sql_var);
    // });
}

// export const deleteCustomer = (customerId) => {
//     return new Promise((resolve, reject) => {
//         let sql = "DELETE FROM customers WHERE customerNumber = ?";
//         let sql_var = [
//             customerId
//         ];

//         functions.executeQuery(resolve, reject, sql, sql_var);
//     });
// }

export default {
    getCustomers,
    getCustomerById,
    getCustomerName,
    getNextCustomerNumber,
    updateCustomer
}