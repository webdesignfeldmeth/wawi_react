import functions from './functions';

export const getCategories = async () => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM categories";
        let sql_var = [];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const saveCategory = (category) => {
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO categories (categoryName, created_at, updated_at) " +
                    "VALUES(?, ?, ?)";
        let sql_var = [
            category.categoryName,
            functions.getDate(),
            functions.getDate()
        ];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const categoryExists = (category)=> {
    return new Promise((resolve, reject) => {
        let sql = "SELECT categoryName FROM categories " +
                    "WHERE categoryName = ?";
        let sql_var = [
            category
        ];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const updateCategory = (category) => {
    return new Promise((resolve, reject) => {
        let sql = "UPDATE categories SET categoryName = ? WHERE id = ?";
        let sql_var = [
            category.categoryName,
            category.id
        ];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

// export const deleteCategory = (id) => {
//     return new Promise((resolve, reject) => {
//         let sql = "DELETE FROM categories WHERE id = ?";
//         let sql_var = [
//             id
//         ];

//         functions.executeQuery(resolve, reject, sql, sql_var);
//     });
// }

export const categoryById = (id) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT categoryName FROM categories " +
                    "WHERE id = ?";
        let sql_var = [
            id
        ];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export default {
    getCategories,
    categoryExists,
    saveCategory,
    updateCategory,
    categoryById
}