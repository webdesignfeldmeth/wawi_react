import functions from './functions';

export const getAssignmentYears = async (assignmentNumber) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT DISTINCT SUBSTR(assignmentCreateDate, 1, 4) AS aYear FROM assignments " +
                    "WHERE SUBSTRING(assignmentNumber, 1, 1) = ? " +
                    "ORDER BY aYear DESC";
        let sql_var = [assignmentNumber];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const getAssignmentsByYear = async (year) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT assignmentCreateDate FROM assignments";
        let sql_var = [year];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const getAssignmentsByYearList = async (year, assignment) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM assignments" +
                    " WHERE SUBSTRING(assignmentNumber, 1, 1) = ?" +
                    " AND SUBSTRING(assignmentCreateDate, 1, 4) = ?" +
                    " ORDER BY assignmentNumber DESC";
        let sql_var = [assignment, year];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export default {
    getAssignmentYears,
    getAssignmentsByYear,
    getAssignmentsByYearList
}