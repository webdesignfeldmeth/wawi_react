import { Connection } from './index';

export const getDate = () => {
    let date = new Date();
    let month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
    let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    date = date.getFullYear() + "-" + month + "-" + day + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return date;
}

export const executeQuery = (resolve, reject, sql, sql_var) => {
    Connection.query(sql, sql_var, (err, results) => {
        if(err) {
            return reject(err);
        }
        resolve(results);
    });
}

export default {
    getDate,
    executeQuery
}