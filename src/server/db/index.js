import * as mysql from 'mysql';
import config from '../config';

import Products from './products';
import ProductCategories from './categories';
import Assignments from './assignments';
import Customers from './customers';
import Categories from './categories';
import Object from './object';

export const Connection = mysql.createConnection(config.mysql);

Connection.connect(err =>  {
    if(err) console.log(err);
})

export default {
    Products,
    ProductCategories,
    Assignments,
    Customers,
    Categories,
    Object
}