import functions from './functions';

const options = [

]

export const getProducts = async (id) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM products";
        let sql_var = [];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const getProductsByCategory = async (id) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM products WHERE (productCategory = ?)";
        let sql_var = [id];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const saveProduct = (product) => {
    return new Promise((resolve, reject) => {
        let sql = "INSERT INTO products (productName, productDescription) " +
                    "VALUES(?, ?)";
        let sql_var = [
            product.productName,
            product.productDescription
        ];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export const productById = (id) => {
    return new Promise((resolve, reject) => {
        let sql = "SELECT * FROM products " +
                    "WHERE productNumber = ?";
        let sql_var = [
            id
        ];
        
        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}


export default {
    getProducts,
    getProductsByCategory,
    saveProduct,
    productById
}