import functions from './functions';

export const insertObject = (
    table,
    attributes,
    ) => {
    return new Promise((resolve, reject) => {
        let columns = "";
        let placeholder = "";
        let values = [];
        let count = 1;
        let separator = ",";
        for(let [key, value] of Object.entries(attributes)) {
            if(count == Object.entries(attributes).length) {
                separator = ""
            }
            columns += `${key}` + separator;
            placeholder += "?" + separator;
            values.push(`${value}`);
            count++;
        }
        let sql = "INSERT INTO " + table + " ("+columns+") VALUES ("+placeholder+")";
        let sql_var = values;

        functions.executeQuery(resolve, reject, sql, sql_var)
    })
}

export const updateObject = () => {
    return new Promise((resolve, reject) => {

    })
}

export const deleteObject = (
    table,
    field,
    value
    ) => {
    return new Promise((resolve, reject) => {
        let sql = "DELETE FROM " + table + " WHERE " + field + " = ?";
        let sql_var = [
            value
        ];

        functions.executeQuery(resolve, reject, sql, sql_var);
    });
}

export default {
    insertObject,
    updateObject,
    deleteObject
}