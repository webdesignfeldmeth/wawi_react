import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class InternLink extends Component<IProps, IState> {

    public render() {
        let btnStdClass = "btn btn-secondary ";
        // let btnClass = this.props.btnClass ? btnStdClass + this.props.btnClass : btnStdClass;
        // let faClass = "fa fa-" + this.props.faClass;
        // let value = this.props.value ? <span>{this.props.value}</span> : "";
        // let disabled = this.props.disabled ? true : false;
        return (
            <Link 
                to={this.props.to}
                >
                <i 
                    // className={faClass}
                    // title={this.props.title}
                    >
                    </i>
                    {/* {value} */}
            </Link>
        )
    }
    
}

export interface IProps {
    btnClass?: string,
    // faClass: string,
    // title?: string,
    // value?: string,
    // valueConfirm?: string,
    // id?: number
    // clickHandler?: any,
    // disabled?: boolean,
    to: string
}

export interface IState {

}

export default InternLink;