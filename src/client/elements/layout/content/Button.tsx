import * as React from 'react';

class Button extends React.Component<IProps, IState> {

    public render() {
        let btnStdClass = "btn btn-secondary ";
        let btnClass = this.props.btnClass ? btnStdClass + this.props.btnClass : btnStdClass;
        let faClass = "fa fa-" + this.props.faClass;
        let display = this.props.display ? <span>{this.props.display}</span> : "";
        let disabled = this.props.disabled ? true : false;
        let dataToggle = this.props.dataToggle ? "modal" : "";
        let dataTarget = this.props.dataTarget ? this.props.dataTarget : "";
        let dataDismiss = this.props.dataDismiss ? this.props.dataDismiss : "";
        return (
            <button 
                className={btnClass} 
                onClick={() => this.props.clickHandler ? this.props.clickHandler(this.props) : null}
                onChange={() => this.props.changeHandler ? this.props.changeHandler(this.props) : null}
                disabled={disabled}
                data-toggle={dataToggle}
                data-target={dataTarget}
                data-dismiss={dataDismiss}
                >
                <i 
                    className={faClass}
                    title={this.props.title}
                    >
                    </i>
                    {display}
            </button>
        )
    }
    
}

export interface IProps {
    faClass: string
    btnClass?: string
    id?: number
    title?: string
    display?: string
    clickHandler?: any
    changeHandler?: any
    disabled?: boolean
    dataToggle?: boolean
    dataTarget?: string
    dataDismiss?: string
    value?: string
}

export interface IState {

}

export default Button;