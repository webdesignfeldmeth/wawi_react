#!/bin/bash

echo "1: React App installieren"
echo "2: React App mit Datenbank installieren"
echo "3: React App (von aussen) starten"
echo "4: Entwicklungsserver starten"
echo "5: Live Version (von aussen) generieren"
echo "6: Live Version generieren"
echo ""
echo "Bitte wählen:"

read OPTION

case "$OPTION" in
	1)
	echo "Projektname"
	read PROJECT
	sudo npm install -g create-react-app
	create-react-app $PROJECT
	cd $PROJECT
	cp ../react.js .
	;;
	2)
	echo "Projectname"
	read PROJECT
	git clone https://github.com/covalence-io/barebones-react-typescript-express.git
	mv barebones-react-typescript-express $PROJECT
	cd $PROJECT
	;;
	3)
	echo "Verfügbare Projekte:"
	ls -d */
	echo "Projektname:"
	read PROJECT
	cd $PROJECT
	npm start -- start
	;;
	4)
	sudo npm run dev
	;;
	5)
	echo "Projektname:"
	read PROJECT
	cd $PROJECT
	npm run build
	;;
	6)
	npm run build
	;;
esac
